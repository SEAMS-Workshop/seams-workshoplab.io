---
layout: page
title: Principles
permalink: /principles/
---
# Focus Topics

The *Principles* course covers five focuses in five days. Those Focuses are:

 - Project Planning & Design
 - Workspace Organization & Tools
 - Reuse & Reusability
 - IO, Big and small
 - High Performance Computing

# Schedule

Each day adopts a different lens on **your work**, and follows the same schedule:

 - warming up,
 - opening with a faculty-led *focus discussion* on core concepts,
 - transitioning to a *applied practical* where participants work a structured problem, and
 - culminating in *project work* devoted to tasks and feedback on **your project**.

The mornings start with [Warmups](/warmup/) followed by the [Focus Topics](/topic/), where the faculty engage participants in the important ideas & practices in the day's topic; these sessions are very interactive, and include several partnered mini-exercises.  In the afternoon, we shift to working more with the keyboard.  First, we complete a [Practical](/practical/) example problem tailored to the day's topic.  Lastly, the participants close out each day working through a series of [project tasks](/project/) applied to *their own projects*; the faculty prepare these challenge lists to exceed what can be accomplished in the available time, so participants will be able to choose what aspects of their work they want pursue in greater depth.