---
layout: page
title: Schedule
permalink: /schedule/
---

{:.schedule-table}
|             Design             |             Workspace             |             Reuse             |             IO             |             HPC             |
|:------------------------------:|:---------------------------------:|:-----------------------------:|:--------------------------:|:---------------------------:|
| [Discussion](/session/design)  | [Discussion](/session/workspace)  | [Discussion](/session/reuse)  | [Discussion](/session/io)  | [Discussion](/session/hpc)  |
| [Practical](/practical/design) | [Practical](/practical/workspace) | [Practical](/practical/reuse) | [Practical](/practical/io) | [Practical](/practical/hpc) |
|   [Project](/project/design)   |   [Project](/project/workspace)   |   [Project](/project/reuse)   |   [Project](/project/io)   |   [Project](/project/hpc)   |
|   [Reference](/topic/design)   |   [Reference](/topic/workspace)   |   [Reference](/topic/reuse)   |   [Reference](/topic/io)   |   [Reference](/topic/hpc)   |

The schedule for SEAMS is divided into two weeks; only the first week applies for the *Principles* course.

Week 1 concerns our five topic focuses (more below), with each day comprising:
 - Warmup, roughly 0830-0930: a quick exercise to get participants thinking
 - Topic Discussion, roughly 0930-1230 with a half hour tea break: interactive discussion of the day's focus.
 - Lunch, 1230-1330
 - Practical, 1330-1500: a guided practical exercise for the day's topic
 - Afternoon tea, 1500-1530
 - Personal Project Work, 1530-1730: participants apply the set of questions from the day's topic to their personal projects

Optional Weekend group activity: Table Mountain Hike, Saturday morning, approximately 9 AM start (to avoid heat) from Kirstenbosch side.

Week 2 concerns coached personal project work. At the end of week 1, participants will develop a plan for week 2.
 - Monday-Thursday: schedule on an individual basis. Every day will have a scheduled chat with one of the faculty (rotating among them) in the morning and afternoon.
 - Friday morning: participants will present their project work to the group

## Topic Focuses

We focus on five topic areas:

 - Project Organization: What is required to accomplish your project? What are the pieces that you'll use? How should you arrange them? How will they work together? How can those choices help (or hinder!) you plan to get work done?
 - Workspace Tools: What tools should you use? Are there some you have to use? How do they help you work with collaborators? Do they demand too much for those collaborators? Do they comply with your project requirements, like data privacy?
 - Reuse & Reusability: Are there parts of your project that someone else (maybe even your past self!) has already implemented? Are there parts that someone else (hopefully, future you!) will want to use? What should you do to make your code more friendly to including other work, and being included elsewhere?
 - Input & Output: What kind of data do you have? How much do you have? What format should you support reading in? Storing output in? What lines in your code currently should be input instead?
 - High Performance Computing: Where is your project stumbling? Too much data, inefficient implementations? If everything is highly tuned, and it's still too much for your personal machine, what next? How you can break up your problem in bits that will work in parallel? How you can write code that will be cluster-computation ready?
