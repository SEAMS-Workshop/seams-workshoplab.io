---
layout: page
title: Contact Us!
permalink: /communications/
---

Email us at: [seams@aims.ac.za](mailto:{{ site.email }}), or contact us via {% include icon-twitter.html username=site.twitter_username -%}!

The repository for the course materials is {% include icon-gitlab.html username=site.gitlab_username -%}, where we happily accept accept issues. For updating personal information, we expect workshop alums will also include a pull request.  For problems with the site, just a thorough description of the error is enough.

## Publications

Past participants: have a publication you want us to recognize (particularly those derived from project work at SEAMS)?  Just let us know via [email](mailto:seams@aims.ac.za) or [@SeamsWorkshop](https://twitter.com/SeamsWorkshop)!  We will add to the site, distribute to other alumni, *et cetera*.

## Acknowledgement Statement

If you think that one of your future publications was substantially influenced by your participation in the SEAMS Workshop, we would appreciate you acknowledging the workshop.  We suggest working from the following phrasing, with additional specifics as appropriate:

> We thank the Software Engineering for Applied Mathematical Sciences (SEAMS) Workshop (link or citation as appropriate) participants and faculty, YEAR and LOCATION, for valuable feedback on early versions of the code underlying this work. In particular, (...description of the major software engineering insights and how they improved the product).