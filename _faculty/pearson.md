---
fullname: Carl A. B. Pearson
goby: Carl
img: carl.jpg
links:
  -
    title: google scholar
    url: https://scholar.google.com/citations?user=XKRJAXwAAAAJ
affiliation:
  -
    org: LSHTM
    position: Research Fellow
  -
    org: SACEMA
    position: Research Fellow
---