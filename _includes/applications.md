{% if site.taking_apps %}Applications {% include lo.md tx='are now open' l=site.taking_apps.url %} for {{site.nextdate}}! Application reviews starting ***{{site.taking_apps.end}}*** - rolling admissions after then; see application site for full details.
{% endif %}
