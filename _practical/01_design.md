---
slug: design
title: Project Design & Planning
---

We're going to get into small groups for a few exercises to get you thinking about design. For all of these exercises, we will give you "prompts" - these describe in plain language some task. The challenge in each problem will not be the task itself, but rather the analysis of the task in terms of requirements, design, and planning.

# Activity 1: Design Telephone

Get into groups of 3 (or more, but not less than 3) and decide on a cycle - who passes to whom, forming a loop. You'll each be given an initial prompt - keep it secret!

First, each of you translate your prompts in _pseudocode_. Once each of you are done, pass the pseudocode to the next person; don't let them see the prompt.

Second, from the pseudocode, each of you write down a _process flow diagram_. Again, once each of you are done, pass the process diagram to the next person; don't let them see the pseudocode.

Third, from the process diagram, each person should write down a plain language description of a task.

Finally, reveal the previous steps back to the original prompt - does the final prompt match the original? Where did the mistranslation occur? Why?

# Activity 2: Design Iteration

Get into new groups, this time of 2-3. Each group will be given prompts in a series, representing an evolving task (akin to what we closed out the discussion session with). For each prompt you will need to prepare a (or revise a previous) _design diagram_ for solving the problem.

How did the prompts evolve? Did the changes have a big impact on your design? If they did, how might you have approached the earlier stages differently?

# Activity 3: Working Collaboratively

Get into new groups of 3-4; each group will be given an overall task, broken into three parts.

For the first pass, each group member will work independently on a part, _without communicating with their team_. Then, the team will try to put their pieces together.

What problems did you encounter putting the pieces together? What might you do differently?

Take a moment to write those down, and to make a plan about how you could deal with them.

Okay, got a plan?

Now we're going trade problems between teams. You team is again to work independently, then put your pieces together - but this time first you get to look at your pieces, chat for short period of time, perhaps write down some things, pick who does which, and then work independently.

When you regroup to assemble your pieces this time, how did things differ? What did *you* do differently, and what effect do you think it had?