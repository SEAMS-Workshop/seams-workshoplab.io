---
slug: reuse
title: Reuse & Reusability Practical
---

## 1. Write an installable hello world package

Write a "hello world" a library/module/package (whatever the term is in your language of choice). The only functionality this package needs to have is a function `helloworld()`, taking no arguments, and printing the phrase "hello world" to the screen. Make sure it works.

Now, research how to package the code in your chosen language, and do it! Here are some resources for [python packages](https://python-packaging.readthedocs.io/en/latest/) and [R packages](http://r-pkgs.had.co.nz/) (also [here](https://r-pkgs.org/)). When you're done, pair up with someone and see if you can successfully install each other's packages. You may want to: 

 1. Have your code available as a repository, so your friend can access it. 
 2. Have a README explaining how to install your package, and how to use it. 

If your partner can't install it without extensive help from you, that may mean your documentation is bad.

## 2. Write a not-so-hello world package

Building from yesterday's practical (organizing some messy code), take your organized code (or work from one of the [instructor solutions](https://github.com/SEAMS-Workshop/workspace_practical)), and turn it into a package. A few considerations:

 - What goes in the "library" part? Why?
 - If something doesn't go in the library part, how are people going to get it otherwise?