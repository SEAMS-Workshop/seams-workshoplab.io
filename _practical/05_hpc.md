---
slug: hpc
title: High Performance Computing
---

## Hello, Hello, Hello ...

Get into pairs, ensuring each group has someone with access [CHPC](http://wiki.chpc.ac.za/).

Submit a size 10 [array job](https://duckduckgo.com/?q=pbs+pro+array+job) to CHPC, for some helloworld of your choice. Have that helloworld code:

 - Greet with "Hello, job N!", where $N$ is the position in the array job.
 - record that result for each point in the array

Collect those resulting greetings, by when they happened in realtime, into a single file.

## Not-so-helloworld

Return to your reuse practical results (or use the instructor [examples](https://github.com/SEAMS-Workshop/reuse_practical)). Remember how we did these analysis for ZAF? Figure out a way to use an array job, plus that existing code, to get the results for several countries in Southern Africa.