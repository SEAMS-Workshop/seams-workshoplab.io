---
slug: design
title: Project Design & Planning
---

# REQUIRED

- Presentation:
    * Copy and revise your slide from the warmup. You can add one other slide. Add it to the folder where you editted the presentation for this morning's warmup.
    * Uses today's the discussion of requirements, design, and planning to help you frame the presentation: what should your project do, how should it work, and what's your plan to make that happen next week?
    * Use at least one visualization (*e.g.*, the flowchart you develop in response to the prompt below) to help explain your project.
    * Spend no more than 20 minutes on this. You may want to do it after you work through some of the optional items below.

# PRIORITIZE THOSE THAT APPLY TO YOUR PROJECT

- Make a record of your requirements
    * For the tasks you complete below, add a record of them to wherever you're keeping your project. For example, they could become part of a README in your repository, a design document, elements of a supplementary information draft, etc.
- Answer the 5WH (Why, Where, Who, What, When and How) questions for your project. E.g. "Who is using the project?" "What do they use it to do?" "How do they use it?" - try to be specific with both question and answers.
- For whatever tool you use to manage your project, try to layout a project plan - big picture milestone steps, detailed tasks associated with achieving those, marked with who needs to do what by when, etc.
- Identify project scope:
    * In 1-3 sentences, what does your project *do* now?
    * Imagine that you have completed this workshop, including the hack-a-thon on your project; write the new version of those 1-3 sentences. Keep in mind: that description might not change at all!  If it won't change, write 1-3 sentences about what *will* change (e.g., performance). 
    * Are there similar projects (that is, performing mostly the same function, but perhaps in a different language or for different inputs) you can learn from or build on?  Do an internet search as needed.
    * Who will interact with your software?  Think broadly about the definition of "interact" - for example, who might see the results? Who else might change the code? Who might run the software?
    * What constraints are there on the user interface?
- I/O specific requirements:
    * What data does your project require (inputs)?
    * Where/how will you obtain it?
    * What are the outputs from your project?
    * What data/file formats will you need to work with?
- Divide and conquer:
    * Identify 3-6 subproblems that your project can be divided into
    * What is the most natural order in which to tackle the subproblems?
        * Easist first?  Hardest?  From the interface in, or following the flow of information?
        * Which can be implemented independently?  Do some absolutely have to be completed first?
    * How will you verify that specific subproblems have been solved?
- Draw a flowchart of the problem your project solves and its parts.
    * Identify which steps each of your detailed requirements apply to.
    * Are the steps that do not have detailed requirements associated with them?
- Technical requirements:
    * What are the external technologies (for example, random number library, SQL database for recording data) your project uses? Or could use? Do an internet search as needed.
    * What do you need to know/learn in order to complete the project?
    * What platforms (*e.g.* OSX, Windows, HPC) should your project work on?
- If the project is substantially completed, what things do you think should have been done differently?
- Validation / Testing:
    * How could you test that specific requirements have been satisfied?  As in, what comparisons would you make (don't worry about what framework would you use)?
    * To adequately & practically test your project, do you need to automate tests?
- Scientific Requirements:
    * What are the specific science-related concerns for your project?
