---
slug: reuse
title: Reuse & Reusability
---

## REQUIRED: Identify a reused/reusable part of your project

Thinking about your project, identify some part of it that either was separately written by you for some other work and reused *OR* that is likely to be so in the future. Was/is/will that part in a library of some sort, or is it a copy-paste-tweak situation? If it's already usable like a library, how easy was that? What improvements might be made? If not, what would need to make it more library-like?

## Reuse of your project

### Users

Thinking about your project as a whole or perhaps some element of it (either present currently, or planned as step outlined in pseudocode or a flow diagram):

   * For your future self: aside from the required item above, how could some other parts of your code be used by yourself in other projects?
   * Beyond you: who are your target users or stakeholders (be as specific as possible)? What about your immediate co-workers? What about other people in the same field? In other fields?
   * For those other people: what is going to make your module attractive to them?  
   * Write a few sentence about what your package does, then use those to search the web for code (in the same or other languages) that provide similar capabilities.  What did you learn?

### Niche / Capabilities

If there is a reusable element, what problem is it solving? Is it a whole new idea? A better performing version of an existing idea? A customization for a particular setting?

### Technical Steps

   * find out how and where packages are distributed in your preferred language, e.g., CRAN for R, _but not exclusively_. Try to identify multiple routes. If relevant, create account(s) with pertinent service(s).
   * Leaning into the workspace and planning skills from previous days: create issues for discrete steps that need to happen to make your project (or elements of it) into a distributable entity.
   *  Research the mechanics of package generation in your preferred language
     - identify a source of guidelines or best practices and bookmark that
     - install tools / template libraries / etc that can help you generate a package; repeat the helloworld package practical from today using those tools. 
   * Recalling what you learned from workspace organization  
     - now that you know more about package generation for your preferred language, are they any particulars about filesystem arrangement?  How do they work with (or against) choices you made yesterday?
     - In addition to directory structure, are there are system configuration elements (e.g., environment variables, program aliases) that are required for packaging? Or for installing the resulting package properly for you intended users? What's available to manage that?
   *  Do an inform pro/con list. Now that you have imagined a package and explored the tools and best practices for generating and distributing packages:
     - what extra work is required for you to make a package? 
     - what are the costs and benefits?
     - where on the spectrum of reuse does your work fall? Is there a small, generic tool? Is it a large pipeline that demonstrates an overall workflow, but would entail lots of tweaking to actually reuse? 

## Parts of your project that might be replaced with existing modules

This assumes that you know how to find packages for your preferred language and platform.  

   *  Identify replaceable components in your project.
     - Thinking back to a pseudocode or flow diagram version of your project, are there steps that you can describe succinctly (in English, as single step in the flow), but that you have done quite a bit of coding to actually accomplish? These are candidates for replacement!
     - List 3+ such points in your project, and write a "requirements" style description of them.
     - Using that description, poke around on the web. Try to find code or a tool (in any language) that does what you want.
     - What extra functionality would you get from this tool over how you do it currently (e.g., handling errors or exceptions)? Is it going to make your life easier? What are some downsides?
   * Map out the steps to replace part of your code with an imported module
     - does this add further dependencies? Is that a problem? How do you decide? 
     - How much rewriting will you have to do to use this tool? E.g. changing the kind of objects passed around (in few places? a lot of places?), changing (vs just deleting) lots of code? 
     - how will you declare this dependency to potential users?
 
## Ways to extend your project with existing modules

This assumes that you know how to find packages for your preferred language and platform.  

   *  Write down 3 ways that you might want to extend your project with new functionality.  Explore ways to implement that functionality using existing modules.  For instance
     - IO.  Maybe you want to store outputs in a database instead of ad hoc files.  Maybe you you want to input JSON instead of ad hoc files.  Are there modules for that?  
     - Estimation methods.  Are there some other ways to do parameter estimation, perhaps more powerful or flexible than what you are doing?  
     - Graphical user interfaces.  Would your code benefit from e.g. a web interface?  What's the easiest way to do that?
   * Thinking about other tools your hypothetical package might be used with - what are their interfaces?  Is your interface written with the same approach?  What new way of thinking would potential users have to learn to combine your library with others?  Are there ways to make your interface more similar to that of other libraries a scientist would commonly use alongside your work?

## Reuse Setting

   * Where do you imagine your code being reused?  As a command line tool, as a package within some larger program, ...?  What are the implications of those settings?

## Testing

   * How testing is relevant to reuse?  If someone wants to extend your work (or you want to extend other work), how can you make sure you don't break that code?