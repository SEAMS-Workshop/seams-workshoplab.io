---
layout: default
---
{::options parse_block_html="true" /}
<div class="home">{% include applications.md %}
Welcome to the ***Software Engineering for Applied Mathematical Sciences*** (***SEAMS*** for short) site!  We are currently in the planning phase for a 2025 workshop!

At SEAMS, we prepare scientists for the challenges of computationally-oriented research program. We have two offerings: the *Principles* course covering our five topic focuses and the *Workshop* which incorporates a hack-a-thon **on your project** in addition to the course material.

We are pleased to announce that SEAMS has become part of the [International Clinics on Infectious Disease Dynamics and Data (ICI3D)](https://www.ici3d.org/). We will be migrating this site and our other materials to that umbrella soon, but we will leave a redirect here once that happens.

The most recent *Workshop* was held in Februrary and March 2023 in partnership with [SACEMA](http://www.sacema.org/) in Stellenbosch, South Africa.  We plan to offer *Workshop* again in February 2025 in partnership with [CEMA](https://cema-africa.uonbi.ac.ke/) in Nairobi, Kenya.

For information on early iterations of the workshop, please see the {% include oldlink.md tx='old website' %}.  [Seest recent participants](participants/dec2018/) and [their feedback on the program](past).

***SEAMS*** has been made possible by contributions from {% include lo.md tx='AIMS' l='https://www.nexteinstein.org/' %} {% include lo.md tx='South Africa' l='https://aims.ac.za/' %} and {% include lo.md tx='Ghana' l='https://aims.edu.gh/' %}, {% include lo.md tx='ONR' l='https://www.onr.navy.mil/' %} and {% include lo.md tx='USAFRICOM' l='https://www.africom.mil' %}, {% include lo.md tx='University of Florida' l='https://biology.ufl.edu/' %}, {% include lo.md tx='SACEMA' l='http://www.sacema.org' %}, and other partners.
</div>
